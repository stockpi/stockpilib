-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 23, 2023 at 06:49 PM
-- Server version: 10.6.14-MariaDB
-- PHP Version: 8.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stockpi01`
--

-- --------------------------------------------------------

--
-- Table structure for table `parts`
--

CREATE TABLE `parts` (
  `id` int(11) NOT NULL,
  `partname` char(64) DEFAULT NULL,
  `remark` char(100) DEFAULT NULL,
  `active1` int(11) DEFAULT -1,
  `brand` varchar(24) DEFAULT NULL,
  `myref` varchar(20) DEFAULT NULL,
  `manufacturer` varchar(36) DEFAULT NULL,
  `manref` varchar(50) DEFAULT NULL,
  `details` varchar(256) DEFAULT NULL,
  `catname` varchar(36) DEFAULT NULL,
  `platf1` varchar(20) DEFAULT NULL,
  `platf2` varchar(20) DEFAULT NULL,
  `platf3` varchar(20) DEFAULT NULL,
  `platf4` varchar(20) DEFAULT NULL,
  `collectionof` varchar(24) DEFAULT NULL,
  `shareprive` char(5) DEFAULT NULL,
  `pricepaid` decimal(7,2) DEFAULT 0.00,
  `pricemarket` decimal(7,2) DEFAULT 0.00,
  `myvalue` decimal(7,2) DEFAULT 0.00,
  `outofmarket` date DEFAULT NULL,
  `suppliername` varchar(30) DEFAULT NULL,
  `supref` varchar(50) DEFAULT NULL,
  `orderdate` date DEFAULT NULL,
  `deliverydate` date DEFAULT NULL,
  `deliveryref` varchar(50) DEFAULT NULL,
  `isavailable` char(1) DEFAULT 'Y',
  `myproject` varchar(30) DEFAULT NULL,
  `upref` bigint(20) DEFAULT -1,
  `downref` bigint(20) DEFAULT -1,
  `linkto` bigint(20) DEFAULT -1,
  `documentation` varchar(256) DEFAULT '',
  `doclinkurl` varchar(256) DEFAULT '',
  `imglinkurl` varchar(256) DEFAULT '',
  `img2linkurl` varchar(256) DEFAULT '',
  `img3linkurl` varchar(256) DEFAULT '',
  `stocklocation` varchar(256) DEFAULT '',
  `outdate` date DEFAULT NULL,
  `outremark` varchar(256) DEFAULT NULL,
  `todelete` char(1) DEFAULT 'N',
  `creat` timestamp NOT NULL DEFAULT current_timestamp(),
  `creby` char(24) DEFAULT NULL,
  `updat` datetime DEFAULT NULL,
  `updby` char(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='parts in stock';

-- --------------------------------------------------------

--
-- Table structure for table `_dbVersion`
--

CREATE TABLE `_dbVersion` (
  `id` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `subversion` int(11) DEFAULT NULL,
  `remark` char(100) DEFAULT NULL,
  `creat` timestamp NOT NULL DEFAULT current_timestamp(),
  `creby` char(24) DEFAULT NULL,
  `updat` datetime DEFAULT NULL,
  `updby` char(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='keep db version';

--
-- Dumping data for table `_dbVersion`
--

INSERT INTO `_dbVersion` (`id`, `version`, `subversion`, `remark`, `creat`, `creby`, `updat`, `updby`) VALUES
(1, 3, 0, 'add collectionof and uitlpriv', '2023-03-15 16:47:44', 'cybr', '2023-07-09 19:15:37', 'cybr');

-- --------------------------------------------------------

--
-- Table structure for table `_dbVersionLog`
--

CREATE TABLE `_dbVersionLog` (
  `id` int(11) NOT NULL,
  `operation` char(32) DEFAULT NULL,
  `remark` char(100) DEFAULT NULL,
  `active1` int(11) DEFAULT NULL,
  `fromversion` varchar(11) NOT NULL DEFAULT '-1',
  `toversion` varchar(11) NOT NULL DEFAULT '-1',
  `appname` varchar(256) DEFAULT NULL,
  `appversion` varchar(256) DEFAULT NULL,
  `creat` timestamp NOT NULL DEFAULT current_timestamp(),
  `creby` char(24) DEFAULT NULL,
  `updat` datetime DEFAULT NULL,
  `updby` char(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci COMMENT='keep track of db and app versions used';

--
-- Dumping data for table `_dbVersionLog`
--

INSERT INTO `_dbVersionLog` (`id`, `operation`, `remark`, `active1`, `fromversion`, `toversion`, `appname`, `appversion`, `creat`, `creby`, `updat`, `updby`) VALUES
(1, 'INITIALISE', 'Initialise', 1, '-1', '1:0', 'dbinfocode', '3.2.9', '2023-03-15 16:47:44', 'cybr', NULL, NULL),
(2, 'INCREASEMAIN', 'add field Brand', 1, '1:0', '2:0', 'dbinfocode', '3.2.9', '2023-03-15 17:25:19', 'cybr', NULL, NULL),
(3, 'INCREASEMAIN', 'add collectionof and uitlpriv', 1, '2:0', '3:0', 'DbInfoCode', '3.2.13', '2023-07-09 17:15:37', 'cybr', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `_dd_helpdata`
--

CREATE TABLE `_dd_helpdata` (
  `id` int(11) NOT NULL,
  `datatype` char(32) DEFAULT NULL,
  `name` char(64) DEFAULT NULL,
  `origin` varchar(128) NOT NULL DEFAULT '',
  `active` int(11) DEFAULT -1,
  `description` varchar(256) DEFAULT '',
  `example` varchar(256) DEFAULT '',
  `defaultvalue` varchar(256) DEFAULT '',
  `creat` timestamp NOT NULL DEFAULT current_timestamp(),
  `creby` char(24) DEFAULT NULL,
  `updat` datetime DEFAULT NULL,
  `updby` char(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `_dd_helpdata`
--

INSERT INTO `_dd_helpdata` (`id`, `datatype`, `name`, `origin`, `active`, `description`, `example`, `defaultvalue`, `creat`, `creby`, `updat`, `updby`) VALUES
(1, 'field', 'partname', 'parts', 1, 'general name, user choosen', 'Raspberry Pi4, Arduino UNO, cinch cable, Retro Soccer game', '', '2023-03-15 16:25:47', 'cybr', NULL, NULL),
(2, 'field', 'active1', 'parts', 1, 'active record=1, inactive=0, unknown=-1', '1, 0, -1', '-1', '2023-03-15 16:29:17', 'cybr', NULL, NULL),
(3, 'field', 'myref', 'parts', 1, 'user choosen ref', 'workshop arduino programmeren', '', '2023-03-15 16:34:02', 'cybr', NULL, NULL),
(4, 'field', 'manufacturer', 'parts', 1, 'Name (or id code) of manufacturer', 'Premier Farnell', '', '2023-03-15 16:34:51', 'cybr', NULL, NULL),
(5, 'field', 'brand', 'parts', 1, 'merk, label, fabrikant', 'Arduino, RaspberryPi', '', '2023-03-28 18:37:31', 'cybr', NULL, NULL),
(6, 'field', 'manref', 'parts', 1, 'reference no or code of the manufacturer', '', '', '2023-03-28 18:39:39', 'cybr', NULL, NULL),
(7, 'field', 'remark', 'parts', 1, 'free remark about the part', '', '', '2023-07-09 17:31:13', 'cybr', NULL, NULL),
(8, 'field', 'details', 'parts', 1, 'properties of the part', '4Gb 250Mhz 3m 5ns', '', '2023-07-09 17:36:24', 'cybr', NULL, NULL),
(9, 'field', 'catname', 'parts', 1, 'Raw categorisation ', 'SBC, microcontroller, sensor, actuator, component, powers', '', '2023-07-09 17:37:52', 'cybr', NULL, NULL),
(10, 'field', 'platf1', 'parts', 1, 'platform originally mainly intended for', '', '', '2023-07-09 17:56:35', 'cybr', NULL, NULL),
(11, 'field', 'platf2', 'parts', 1, 'platform also intended for', 'raspberry pi, arduino, micro:bit', '', '2023-07-09 17:57:09', 'cybr', NULL, NULL),
(12, 'field', 'platf3', 'parts', 1, 'platform also intended for', 'raspberry pi, arduino, micro:bit', '', '2023-07-09 17:57:15', 'cybr', NULL, NULL),
(13, 'field', 'platf4', 'parts', 1, 'Connector platform', 'Grove, JST SH, JST PH, jumper, ', '', '2023-07-09 17:57:30', 'cybr', NULL, NULL),
(14, 'field', 'collectionof', 'parts', 1, 'owned by', '', '', '2023-07-09 17:57:39', 'cybr', NULL, NULL),
(15, 'field', 'shareprive', 'parts', 1, 'available for others to share or private use only', 'share, prive ', '', '2023-07-09 18:17:12', 'cybr', NULL, NULL),
(16, 'field', 'pricepaid', 'parts', 1, 'purchase price 0=unknown', '45.50', '', '2023-07-09 18:19:23', 'cybr', NULL, NULL),
(17, 'field', 'pricemarket', 'parts', 1, 'marketprice to compare your bargains', '41.50', '', '2023-07-09 18:20:42', 'cybr', NULL, NULL),
(18, 'field', 'myvalue', 'parts', 1, 'other value eg for calculation of stockvalue', '44', '', '2023-07-09 18:22:00', 'cybr', NULL, NULL),
(19, 'field', 'outofmarket', 'parts', 1, 'not for sale anymore since', '', '', '2023-07-09 18:22:21', 'cybr', NULL, NULL),
(20, 'field', 'suppliername', 'parts', 1, 'supplier name', 'MyElectronicsSite', '', '2023-07-09 18:22:37', 'cybr', NULL, NULL),
(21, 'field', 'supref', 'parts', 1, 'reference used by supplier', 'code on site, deliverynote, invoice', '', '2023-07-09 18:23:32', 'cybr', NULL, NULL),
(22, 'field', 'orderdate', 'parts', 1, 'date of ordering the part (to calculate delay)', '01/05/2022', '', '2023-07-09 18:24:19', 'cybr', NULL, NULL),
(23, 'field', 'deliverydate', 'parts', 1, 'Date of arrival of ordered part', '14/5/2022', '', '2023-07-09 18:24:42', 'cybr', NULL, NULL),
(24, 'field', 'deliveryref', 'parts', 1, 'find back parts in joint shipment', '2022-05-06-123456', '', '2023-07-09 18:28:25', 'cybr', NULL, NULL),
(25, 'field', 'isavailable', 'parts', 1, 'current state of the part used in a project is N', 'Y, N (empty=unknown)', '', '2023-07-09 18:29:53', 'cybr', NULL, NULL),
(26, 'field', 'myproject', 'parts', 1, 'name of project part is related to, bought for, used in', 'StockPi-1', '', '2023-07-09 18:32:34', 'cybr', NULL, NULL),
(27, 'field', 'upref', 'parts', 1, 'free refence or id to other part up in hierarchy', '', '', '2023-07-09 18:33:39', 'cybr', NULL, NULL),
(28, 'field', 'downref', 'parts', 1, 'reference to object down in hierarchy ', '', '', '2023-07-09 18:34:51', 'cybr', NULL, NULL),
(29, 'field', 'linkto', 'parts', 1, 'URL to part site?', 'www.mfg.com/mypart', '', '2023-07-09 18:37:00', 'cybr', NULL, NULL),
(30, 'field', 'documentation', 'parts', 1, 'describe available documentation sources', 'leaflet, printed manual, site ..', '', '2023-07-09 18:37:18', 'cybr', NULL, NULL),
(31, 'field', 'doclinkurl', 'parts', 1, 'URL for documentation', 'www.mfg.com/mypart/doc', '', '2023-07-09 18:37:45', 'cybr', NULL, NULL),
(32, 'field', 'imglinkurl', 'parts', 1, 'URL for (main) image of part', 'imgPart001.jpg', '', '2023-07-09 18:38:50', 'cybr', 2023-12-13 17:43:50, 'cybr'),
(33, 'field', 'img2linkurl', 'parts', 1, 'URL for (back) image of part', 'imgPart001-back.jpg', '', '2023-12-13 17:43:50', 'cybr', NULL, NULL),
(34, 'field', 'img3linkurl', 'parts', 1, 'URL for (packing) image of part', 'imgPart001-packing.jpg', '', '2023-12-13 17:43:50', 'cybr', NULL, NULL),
(35, 'field', 'stocklocation', 'parts', 1, 'location in stock', 'rack5 board3 box4', '', '2023-12-13 17:43:50', 'cybr', NULL, NULL),
(36, 'field', 'outdate', 'parts', 1, 'Date the part went out of use or possess', '12/12/2022', '', '2023-07-09 18:40:10', 'cybr', NULL, NULL),
(37, 'field', 'outremark', 'parts', 1, 'Reason why part went out', 'sold, broken, stolen, lost, worn out', '', '2023-07-09 18:40:41', 'cybr', NULL, NULL),
(38, 'field', 'todelete', 'parts', 1, 'Mark for delete with Y', 'Y, N or empty', '', '2023-07-09 18:42:40', 'cybr', NULL, NULL),
(39, 'field', 'creat', 'parts', 1, 'date of creation of record in database', '', '', '2023-07-09 18:43:10', 'cybr', NULL, NULL),
(40, 'field', 'creby', 'parts', 1, 'created by user or application', '', '', '2023-07-09 18:43:27', 'cybr', NULL, NULL),
(41, 'field', 'updat', 'parts', 1, 'date of last update of record', '', '', '2023-07-09 18:43:38', 'cybr', NULL, NULL),
(42, 'field', 'updby', 'parts', 1, 'user or application of last update of record', '', '', '2023-07-09 17:41:51', 'cybr', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parts`
--
ALTER TABLE `parts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_dbVersion`
--
ALTER TABLE `_dbVersion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_dbVersionLog`
--
ALTER TABLE `_dbVersionLog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_dd_helpdata`
--
ALTER TABLE `_dd_helpdata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parts`
--
ALTER TABLE `parts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `_dbVersion`
--
ALTER TABLE `_dbVersion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `_dbVersionLog`
--
ALTER TABLE `_dbVersionLog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `_dd_helpdata`
--
ALTER TABLE `_dd_helpdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
