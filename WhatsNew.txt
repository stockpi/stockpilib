What's new for the developer-user of this library

0.4.5 CopyObject : delete id and keep data; save (should) create(s) new record
0.4.0 fix empty database (table parts)
0.2.0 Delete part
0.1.9 MCamera: getCameraCommandOf, getCamDir, setCamDir, setCamExtention, getCamExtention, MStockpilib: toFilename
0.1.8 Test show available camera's with description
0.1.7 MCamera: init, choice from array, set, get settings..
0.1.6 MStockpiData.getDistinct: skip empty and NULL values
0.1.5 MStockpilib: Emtpy the database, MCamera: for different types of cams code

(older: see Changes.txt)

