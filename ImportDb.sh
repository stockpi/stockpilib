#/bin/bash
# working on 2023 08 01 - needs chmod+x ImportDb.sh to execute
echo "$0 to import database structure for Stockpi project;"
echo "mysql -u root -p stockpi01 < stockpi.sql"
echo "Check that your database is running. Tip: sudo systemctl mysql start "
echo "trying to import as root (change if needed) - give database-root password"
mysql -u root -p stockpi01 < stockpi.sql
