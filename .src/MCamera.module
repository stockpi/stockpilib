' Gambas module file

Export 

' Contains the camera choice and handling for stockpi project

Public arrCameras As String[]
Public arrDescriptions As String[]

Private $sLastError As String

Public Sub _init()
  
  initCameraChoice()
  
End

Public Sub getLastError() As String
  
  Return $sLastError
  
End

Public Sub initCameraChoice() As Boolean
  ' only to be run once at install 
  arrCameras = New String[] ' alfabetical
  arrDescriptions = New String[]
  
  arrCameras.Add("directcommand") '  in case no existing one can be used, own command can be stored here
  arrDescriptions.Add("Custom command for unknown camera")
  If Not Settings["Cam/DirectCommand"] ' prepare empty slot for it
    Settings["Cam/DirectCommand"] = ""
    Settings.Save
  Endif 
  arrCameras.Add("libcamera") 
  arrDescriptions.Add("default on newer Raspberry Pi OS Debain 11 Bullseye")
  arrCameras.Add("raspistill") 
  arrDescriptions.Add("default on older Raspberry Pi with eg system 9")
  arrCameras.Add("streamer") '
  arrDescriptions.Add("default on laptops") 
  'arrCameras.Add("other")
  'arrDescriptions.Add("other")
  If Not (Settings["Cam/Current"])
    setCameraChoice("")
    Settings.Save
  Endif 
  If getCamDir() = ""
    If Exist(User.Home &/ "Pictures") Then
      setCamDir(User.Home &/ "Pictures")
    Else 
      setCamDir(User.Home)
    Endif 
  Else 
    $sLastError = "Cannot initialise as value exists: " & getCamDir()
  Endif 
End

Public Sub setCameraChoiceFromArray(iIndex As Integer) As Boolean
  
  If (iIndex < arrCameras.Count) And Not (iIndex < 0) Then
    Try setCameraChoice(arrCameras[iIndex])
    If Error Then
      $sLastError = Error.Text
      Return False
    Else 
      Return True
    Endif
  Else 
    $sLastError = iIndex & " is outside camera's available " & arrCameras.Count
  Endif
  
End

Public Sub setCameraChoice(sCamera As String) As Boolean
  
  Print arrCameras.Find(sCamera)
  If arrCameras.Find(sCamera) >= 0
    Settings["Cam/Current"] = sCamera
    Settings.Save
    Return True
  Else 
    $sLastError = sCamera & " not found in list of " & arrCameras.Count & " camera's"
    Return False
  Endif 
  
End

Public Sub getCameraChoice() As String
  
  Return Settings["Cam/Current"]
  
End

Public Sub setCameraCommand(sCommand As String) ' regular, from choosen camera's
  ' with image path but without filename
  Settings["Cam/Command"] = sCommand
  Settings.Save
  
End 

Public Sub getCameraCommand() As String
  
  Return Settings["Cam/Command"]
  
End

Public Sub getCameraCommandOf(sCamera As String) As String
  
  Select sCamera
    Case "directcommand"
      Return getCameraDirectCommand()
    Case "libcamera"
      Return libcameraCommand()
    Case "raspistill"
      Return raspistillCommand()
    Case "streamer"
      Return streamerCommand()
    'Case ... 
  End Select
  
End

Public Sub setCameraDirectCommand(sCommand As String) ' irregular, outside of camera's known

  Settings["Cam/DirectCommand"] = sCommand
  
End

Public Sub getCameraDirectCommand() As String
  
  Return Settings["Cam/DirectCommand"]
  
End

Public Sub getCamDir() As String
  
  Return Settings["Cam/Imagedir", User.Home]
  
End

Public Sub PictureUrlFromName(sCameraPicture As String) As String


  If (sCameraPicture)
      Return getCamDir() &/ sCameraPicture
  Else 
    $sLastError = "Empty picture name given"
    Return ""
  Endif 

End


Public Sub setCamDir(s As String)
  
  Settings["Cam/Imagedir"] = s
  Settings.Save
  
End

Public Sub getCamExtention() As String
  
  Return Settings["Cam/Extention", "jpeg"]
  
End

Public Sub setCamExtention(sExtention As String)
  
  Settings["Cam/Extention"] = sExtention
  
End

' init camera settings

Public Sub libcameraInit() As Boolean
  ' to test the camera, use on commandline: "libcamera -hello -t 0"
  Settings["Libcamera/sTime"] = "-t 1000"  ' in milliseconds 
  Settings["Libcamera/sFlipVertical"] = "" ' "-- vflip"
  Settings["Libcamera/sFlipHorizontal"] = "" ' "-- hflip"
  Settings["Libcamera/sWidth"] = "" ' "--width 1920"
  Settings["Libcamera/sHeight"] = "" ' "--height 1080"
  'Settings["Libcamera/"] = ""
'          Settings["Libcamera/"] = ""  
  Settings.Save
  Return True
  
End

Public Sub libcameraCommand() As String
  
  Dim sCameraCommand As String

  sCameraCommand = " "
  sCameraCommand &= Settings["libcamera/sTime", "-t 1000"] ' "-t 1000"
  sCameraCommand &= IIf(Settings["libcamera/sFlipVertical", ""], " " & Settings["libcamera/sFlipVertical"], "")
  sCameraCommand &= IIf(Settings["libcamera/sFlipHorizontal", ""], " " & Settings["libcamera/sFlipHorizontal"], "")
  sCameraCommand &= IIf(Settings["libcamera/sWidth", ""], " " & Settings["libcamera/sWidth"], "")
  sCameraCommand &= IIf(Settings["libcamera/sHeight", ""], " " & Settings["libcamera/sHeight"], "")
  sCameraCommand &= " " & "-o " & getCamDir() & "/"
  
  Return sCameraCommand
  
End


Public Sub raspistillInit() As Boolean
 
  Settings["Raspistill/sTime"] = "-t 1" ' "-t 1 "
  Settings["Raspistill/sFlipVertical"] = "" ' "-vf"
  Settings["Raspistill/sFlipHorizontal"] = "" ' "-hf "
  Settings.Save
  Return True
  
End

Public Sub streamerInit() As Boolean
'  Try Shell "streamer --help"
 
  Settings["Streamer/sFormat"] = "-f jpeg"
  Settings["Cam/FormatExtention"] = "jpeg"
  Settings.Save
  Return True
  
End

Public Sub raspistillCommand() As String
  
  Dim sCameraCommand As String

  sCameraCommand = "raspistill "
  sCameraCommand &= Settings["Raspistill/sTime", "-t 1"] ' "-t 1"
  sCameraCommand &= " " & Settings["Raspistill/sFlipVertical", "-vf"] ' "-vf"
  sCameraCommand &= " " & Settings["Raspistill/sFlipHorizontal", "-hf"] ' "-hf"
  sCameraCommand &= " " & "-o " & getCamDir() & "/"
  Return sCameraCommand

End


Public Sub raspistillPicture(sNewItemPicture As String) As Boolean
  ' for testing
  Dim sCameraCommand As String

  sCameraCommand = raspistillCommand() & sNewItemPicture
  Debug sCameraCommand
  Try Shell sCameraCommand
  If Error Then
    $sLastError = Error.Text
    Return False
  Else 
    Wait Settings["Cam/fDelay", 0.9]
    If checkPicture(sNewItemPicture) Then
      Return True
    Else 
      Return False
    Endif
  Endif
  
End

Public Sub checkPicture(sPictureFile As String) As Boolean
  
  If Exist(Settings["Cam/sImageDir", User.Home] & "/" & sPictureFile) Then
    Return True
  Else 
    $sLastError = "Picture file was not found in (not created?) " & Settings["Cam/sImageDir", User.Home] & "/" & sPictureFile
    Return False
  Endif
  
End


Public Sub streamerCommand() As String
  
  Dim sCameraCommand As String
  
  sCameraCommand = "streamer "
  sCameraCommand &= Settings["Streamer/sFormat", "-f jpeg"]
  sCameraCommand &= " -o " & getCamDir() & "/"
  'sCameraCommand &= Settings[]
  Return sCameraCommand
  
End


Public Sub streamerPicture(sNewItemPicture As String) As Boolean
  ' for testing   ' not accepted: jpg, needs jpeg as extention
  Dim sCameraCommand As String
  
  sCameraCommand = streamerCommand() & sNewItemPicture 
  Debug sCameraCommand
  Try Shell sCameraCommand
  If Error Then
    $sLastError = Error.Text
    Debug $sLastError
    Return False
  Else 
    Wait Settings["Cam/fDelay", 0.9]
    If checkPicture(sNewItemPicture) Then
      Return True
    Else 
      Return False
    Endif
  Endif
  
End

Public Sub makePicture(sFileName As String) As Boolean

  Dim sCommand As String
  
  sCommand = getCameraCommand()
  If (sCommand) Then
    Debug sCommand
    sCommand &= sFileName
    $sLastError = sCommand
    Try Shell sCommand
    If Error Then
      $sLastError = Error.Text
      Debug Error.Text
      Return False
    Else 
      Return True
    Endif
  Else 
    $sLastError = "Did not get camera command (empty). No camera choosen? Check config file."
    Return False
  Endif
  
End
