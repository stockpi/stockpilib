# StockpiLib
Library for Stockpi Project

## Installation
Install Linux, install Gambas3
Use git pull and open in Gambas3 IDE.
Project, make executable makes it available for other projects to import as library.

## Usage
Get this file and mark it in the Gambas IDE as a project/library in applications that use it (StockpiCLI and StockpiApp)

## Support
Issue tracker, ..

## Roadmap
Must supply all needed funtions for StockpiCLI and StockpiApp

## Contributing
Currently we werk together on this project in the radioshow webgang on Radio Centraal, any help welcome.

## Authors and acknowledgment
Wim.webgang, Marthe, Silvia

## License
GPL 3

## Project status
Active, partly working, not finished. 
Some camera's work: raspistill, streamer
Working on libcamera, ...
